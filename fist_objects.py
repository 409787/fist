#!/usr/bin/python
#https://www.juniper.net/documentation/en_US/junos/topics/topic-map/security-address-books-sets.html
from enum import Enum
import math
import json
import yaml
import os

#--------- Helper functions --------
def ansibleHandle( directory, tags):
    """Run ansible-playbook with specified tags, return the return value.

    directory -- the root directory of the playbooks - where manage-fw.yml lives
    tags -- which tags should be run, see manage-fw.yml to determine This
    """
    return os.system('ansible-playbook -i '+directory+'../ansible-setting/ansible_inventory --private-key '+directory+'../ansible-setting/private_key  '+directory+'manage-fw.yml -t '+ tags)

def netmaskToSlash(netmask):

    splited=netmask.split('.')
    result=0
    for i in splited:
        #find a better way then round (not fool-proof due to )
        if( int(i) > 255 ):
            raise Exception("YOU SHALL NOT COMPUTE")
        bits=int(math.log((int(i)+1),2))
        result += bits
        if(bits != 8):
            break
    return result

def slashToNetmask(a):
    """Provides transition between the slash notation of netmask and bitwise one.
    Input: integer of in range 0..32 (without the slash itself!)
    Ouput: string of netmask in form of [0..255].[0..255].[0..255].[0..255]
    Example: given netmask of a B-class network e.g.: 147.251.0.0/16 the output will look like:
    255.255.0.0; corresponding wildcard is 0.0.255.255
    >>> slashToNetmask(16)
    255.255.0.0
    """
    if(a<0 or a>32):
        print("Input netmask is out of range for IPv4, quitting with an exit code of 1")
        exit(1)
    res=""
    for i in range(4):
        if(a>8):
            res=res+"255."
            a=a-8
        else:
            res+=str(256-(2**(8-a)))+"."
            a=0
    res=res[:-1]
    return res

# determine if the IP addr with subnet is forwarded / belong here
def isForwardedHere( routIP, routNET, realIP, realNET ):
    """
    Determine if the IP addr or subnet is routed into the subnet
    usage:
    isForwardedHere( "147.251.54.0", "24", "147.251.54.224", "32" ) -> True
    """
    #we need to forbid smaller realNET than routNET - that can never work
    if( int(routNET) > int(realNET) ):
        return False
    #we will work with wildcard
    #TODO: add magic to get wildcard
    # -> DONE, remove this comment if it shows that is working as expected

    routNetmask = slashToNetmask(int(routNET))
    realNetmask = slashToNetmask(int(realNET))

    wildcard=slashToNetmask(int(routNET)).split('.')
    for i in range(4):
        wildcard[i] = str( 255 - int(wildcard[i]) )

    real=realIP.split('.')
    rout=routIP.split('.')
    for i in range(4):
        if(wildcard[i]=='0'):
            if(real[i]!=rout[i]):
                return False
        else:
        #their difference canot be larger than the value of netmask, with first bits proceeding
            difference=int(real[i]) - int(rout[i])
            if(difference > int(wildcard[i]) or difference < 0):
                return False
    return True

#---- End of Helper functions ------

class NetObjType(Enum):
    subnet = 1
    range = 2
    host = 3


class Firewall(object):

    def __init__(self, fwType, execDir , logger, optimizationLevel):
        self.execDir = execDir
        self.type = fwType
        self.logger = logger
        self.optimizationLevel = optimizationLevel
    def getFWObjects(self):
        return 0
    def getRoutInfo():
        return 0

    def createServices( self, servToDeploy ):
        servicesDeployed=[]
        for service in servToDeploy:
            s=service.split(',')
            toReturn = object()
            if s[1] is "icmp":
                if( self.__class__.__name__ == 'FirewallCisco' ):
                    toReturn = IcmpObjCisco('perun'+s[1]+s[0], s[1],s[0])
            else:
                if( self.__class__.__name__ == 'FirewallCisco' ):
                    toReturn = ServiceObjCisco('perun'+s[1]+s[0], s[1], s[0])
            servicesDeployed.append(toReturn)
        self.services = servicesDeployed
        return 0

    def createNetObjectsFromDsts( self, perunDests ):
        counter = 0
        netObjsJuniper = []
        finalZones = []
        for dst in perunDests:
            interface = self.getInterfaceToRout(dst)
            targetZone = self.getZoneOfInterface(interface)
            #FIX?: permanent fallback to global zone zonebook
            finalZones.append((dst,targetZone))
            #break;
        # refactor - merge these for cycles
        listOfNetObjs=[]
        for finalZone in finalZones:
            dstAsAddrWithMask = finalZone[0].split('/')
            if len(dstAsAddrWithMask) == 1:
                dstAsAddrWithMask.append("32")
            #find addrbook based on zone

            bookName = ""
            if(self.__class__.__name__ == 'FirewallJuniper'):
                if finalZone[1] is not '':
                    bookName = self.mapZoneBook[finalZone[1]]
                if bookName is '':
                    bookName="global"
                tmpNetObj = NetObjJuniper( "perunObj"+dstAsAddrWithMask[0]+"slash"+dstAsAddrWithMask[1],
                                        dstAsAddrWithMask[0],
                                        slashToNetmask(int(dstAsAddrWithMask[1])),
                                        bookName )
            elif( self.__class__.__name__ == 'FirewallCisco' ):
                tmpNetObj = NetObjCisco( "perunObj"+dstAsAddrWithMask[0]+"slash"+dstAsAddrWithMask[1],
                                        dstAsAddrWithMask[0],
                                        slashToNetmask(int(dstAsAddrWithMask[1])),
                                        bookName )
            #we need to keep track of the zone - adrbook is not enough
            listOfNetObjs.append((tmpNetObj, finalZone[1]))
        self.objectsFromDestination = listOfNetObjs

    def createNetObjectsFromSource (self, perunSrcs , srcNames ):

        sourcesAsObjects = []
        tuplesZoneSources = []
        for source in perunSrcs:
            #perunSrcs is a list of lists, we need to determine whether every object of inner list is from the same zone
            helpArrayOfZones=[]
            for item in source:
                interface = self.getInterfaceToRout(item)
                zone = self.getZoneOfInterface(interface)
                #print(zone)
                helpArrayOfZones.append(zone)
            #sort src (list of sources) according to values of helpArrayOfZones
            #then split into subs
            if(helpArrayOfZones[1:] is helpArrayOfZones[:-1]):
                #prob. could be removed, need to be tested, or not (because of cisco)
                tuplesZoneSources.append( source, helpArrayOfZones.pop() )
                continue

            #print(source)
            src = [x for _,x in sorted(zip(helpArrayOfZones,source))]
            helpArrayOfZones = sorted(helpArrayOfZones)
            masterArray = []
            whereStuffChange = [ i for i, (x, y) in enumerate(zip(helpArrayOfZones[:-1],helpArrayOfZones[1:])) if x!=y]
            whereStuffChange.reverse()

            for index in whereStuffChange:
                subList = source[index+1:]
                zone = helpArrayOfZones.pop()
                masterArray.append((subList,zone))
                helpArrayOfZones = helpArrayOfZones[:index+1]
                source=source[:index+1]

            #append last item
            masterArray.append((source,helpArrayOfZones.pop()))

            tuplesZoneSources.append(masterArray)
        tuplesZoneSourcesNames=dict(zip(srcNames, tuplesZoneSources))
        #create NetObj
        allGroups = []
        for name in tuplesZoneSourcesNames.keys():
            i = tuplesZoneSourcesNames[name]
            if type(i) is tuple:
                #create a single address-set
                #j is a single addr inside list inside tuple
                groupAsList = []
                for j in i[0]:
                    #find a bookName
                    singleAdrAsObj = self.createSingleNetObj(j ,i[1])
                    groupAsList.append(singleAdrAsObj)
                allGroups.append(groupAsList)
            elif type(i) is list:
                counter = 0
                #create multiple address sets
                for p in i:
                    groupAsList = []
                    for j in p[0]:
                        #find a bookName
                        singleAdrAsObj = self.createSingleNetObj(j, p[1])
                        groupAsList.append( singleAdrAsObj )
                    #
                    if( self.__class__.__name__ == 'FirewallJuniper' ):
                        allGroups.append( NetGroupObjJuniper( name +"-" +str(counter), groupAsList, self.mapZoneBook[p[1]] ))
                    elif( self.__class__.__name__ == 'FirewallCisco' ):
                        allGroups.append( NetGroupObjCisco( name +"-" +str(counter), groupAsList, "" ))
                    counter+=1
        self.objectsFromSources = allGroups
    def getZoneInfo():
        return 0
    def loadPerunObjects():
        return 0
    def setFWObjects():
        return 0
    def deployNetObjs():
        return 0
    def deployServices():
        return 0
    def deployPolicies():
        return 0

    def deployRemote( self , perunRules ):
        self.mergePerunIntoFW( perunRules )
        self.optimizeRules()
        self.deployNetObjs()
        self.deployServices()
        self.deployPolicies()
        self.deployRemoteHard()
        return 0

    def optimizeRules( self ):
        #TODO: fix conter reassignment for the "Juniper" (not based on the numbers but "before" keyword) <- VERIFY_DONE
        optimizedPolicies = []
        for policy in self.accessGroups:
            pol = policy.rules
            if   ( self.optimizationLevel == 0 ):
                pass
            elif ( self.optimizationLevel == 1 ):
                pass
            elif ( self.optimizationLevel == 2 ):
                #ruleset =
                if (len(pol[1]) > 0):
                    counter = pol[1][0].position
                    #skip the counter part for the juniper
                    pol[1] = self.optimizeSetFromOther( pol[0], pol[1] )
                    pol[1] = self.optimizeSingleSet( pol[1] )
                    pol2 = []
                    for i in pol[1]:
                        i.position = counter
                        pol2.append(i)
                        if (self.__class__.__name__ == "FirewallCisco"):
                            #only cisco changes line num. Junos has only 'before'
                            counter += 1
                    pol[1] = pol2
                pass
            elif ( self.optimizationLevel == 3 ):

                for i in pol[0]:
                    if (self.__class__.__name__ == 'FirewallCisco'):
                        self.to_delete.append("no "+i.print())
                    elif( self.__class__.__name__ == 'FirewallJuniper'):
                        self.to_delete.append( "delete security policies from-zone " + policy.from_zone + " to-zone " + policy.to_zone + " policy " + i.name)

                pol[1] = self.optimizeSetFromOther( pol[0], pol[1] )
                pol[1] = self.optimizeSingleSet( pol[1] )
                pol[0] = self.optimizeSingleSet( pol[0] )

                pol1 = []
                pol2 = []
                counter = 1
                if (self.__class__.__name__ == "FirewallCisco"):
                    counter = 1
                elif(self.__class__.__name__ == "FirewallJuniper"):
                    if( len(pol[1]) > 0 ):
                        counter = pol[1][0].position


                for i in pol[0]:
                    i.position = counter
                    pol1.append(i)
                    if (self.__class__.__name__ == "FirewallCisco"):
                        counter += 1

                if (self.__class__.__name__ == "FirewallCisco"):
                    counter += 1

                for i in pol[1]:
                    i.position = counter
                    pol2.append(i)
                    if (self.__class__.__name__ == "FirewallCisco"):
                        counter += 1
                pol[0] = pol1
                pol[1] = pol2
                pass
            elif ( self.optimizationLevel == 4 ):
                #Todo: Recalc Lines
                counter = 1

                if (self.__class__.__name__ == "FirewallCisco"):
                    counter = 1
                elif(self.__class__.__name__ == "FirewallJuniper"):
                    if( len(pol[1]) > 0 ):
                        counter = pol[1][0].position

                ruleset = pol[0] + pol[1]
                ruleset = self.optimizeSingleSet( ruleset )

                ruleSetRecalculated = []

                for i in ruleset:
                    i.position = counter
                    ruleSetRecalculated.append(i)
                    if (self.__class__.__name__ == "FirewalCisco"):
                        counter += 1
                pol[0] = ruleset
                pol[1] = []

            policy.rules = pol
            optimizedPolicies.append(policy)
        self.accessGroups = optimizedPolicies

    def optimizeSetFromOther( self, ruleset1, ruleset2 ):
        """
        Search for optimization in the rulesets, altering only the secon one (based on first one)
        """
        rulesetCopy = ruleset2
        for i in ruleset1:
            rulesetCopy = [j for j in rulesetCopy if not self.isShadowed(i,j)]

        return rulesetCopy

    def optimizeSingleSet( self, ruleset ):
        """
        Search for optimization in a single set, affected by shadowing and redundancy
        """

        rulesetCopy = ruleset
        #rulesetCopy2 = rulesetCopy
        toReturn = []
        for i in rulesetCopy:
            wasRedundant = False
            for j in rulesetCopy:
                if(i==j):
                    continue
                if(self.isRedundant(i,j)):
                    wasRedundant = True
                    break
            if (not wasRedundant):
                toReturn.append(i)

            #rulesetCopy = [j for j in rulesetCopy if not self.isShadowed(i,j)]
            #TODO: add handling of the "isRedundant"
        return toReturn

    def isRedundant( self, rule1, rule2 ):
        """
        Return True if the first rule is contained in the second one (reverse shadowing)
        """
        return self.isShadowed(rule2, rule1)

class FirewallCisco(Firewall):

    def __init__(self, fwType, execDir , logger, optimizationLevel):
        self.execDir = execDir
        self.type = fwType
        self.logger = logger
        self.routes = []
        self.originalObjects = []
        self.optimizationLevel = optimizationLevel
        self.to_delete = []
        if(optimizationLevel == 0):
            #early evaluation of items to delete
            self.to_delete.append("clear configure access-list")

    def isShadowed( self, rule1, rule2):
        """
        Return True if the second rule is shadowed by the first one.
        """
        self.logger.debug("Check Shadowing - Sources: %s and %s \n"
                          "Destinations %s and %s \n"
                          "Services %s and %s"
                          , rule1.source, rule2.source,
                          rule1.destination, rule2.destination,
                          rule1.service, rule2.service
                          )
        if(rule1.permit != rule2.permit):
            self.logger.info("Rules does match their deny/permit property, possibly correlation - Check manually ")
            self.logger.info("(possible correlation) Check manually: - Sources: %s and %s \n"
                          "Destinations %s and %s \n"
                          "Services %s and %s"
                          , rule1.source, rule2.source,
                          rule1.destination, rule2.destination,
                          rule1.service, rule2.service
                          )
            #possible TODO: check whether the rules overlaps/shadows
            return False
        if( self.isObjSuperSet( rule1.source, rule2.source)           and
            self.isObjSuperSet( rule1.destination, rule2.destination) and
            self.isServiceSuperSet( rule1.service, rule2.service)     ):
            self.logger.info("Rule is shadowed by another, and will be deleted:\n"
                              "%s \n"
                              "shadowed: %s\n",rule1.print(),rule2.print())

            return True

        return False
    def isObjSuperSet(self, obj1, obj2 ):
        """
        Returns True if the first object (obj1) is the super set of the second one
        """

        obj1Splitted = obj1.split(" ")
        if(len(obj1Splitted)==1 and obj1Splitted[0] != "any"):
            obj1Splitted.insert(0,"object")
        obj2Splitted = obj2.split(" ")
        if(len(obj2Splitted)==1 and obj2Splitted[0] != "any"):
            obj2Splitted.insert(0,"object")

        #for exhaustive debug uncomment following line:
        #self.logger.debug("Checking whether %s is super set of the %s", obj2, obj2)

        if(obj1Splitted[0] =="object" or obj1Splitted[0] =="object-group"):
            obj1AsObject = self.findObject(obj1Splitted[1])
        elif( obj1Splitted[0] == "any" ):
            return True
            #Early accept - (this is fine, any is superset of anything)
            obj1AsObject = [{'ip1': "0.0.0.0"[0], 'ip2': "0.0.0.0"}]
        elif( obj1Splitted[0] == "host"):
            obj1AsObject = [{'ip1': obj1Splitted[1], 'ip2': "255.255.255.255"}]
        else:
            #TODO: if host ... obj1Splitted[1]  = 255.255.255.255... <- VERIFY_DONE
            obj1AsObject = [{'ip1': obj1Splitted[0], 'ip2': obj1Splitted[1]}]

        if(obj2Splitted[0] =="object" or obj2Splitted[0] =="object-group"):
            obj2AsObject = self.findObject(obj2Splitted[1])
        elif( obj2Splitted[0] =="any" ):
            return False
            #If the first object was not any -> drop this
            obj2AsObject = [{'ip1': "0.0.0.0"[0], 'ip2': "0.0.0.0"}]
        elif( obj2Splitted[0] == "host"):
            obj2AsObject = [{'ip1': obj2Splitted[1], 'ip2': "255.255.255.255"}]
        else:
            #TODO: if host ... obj2Splitted[1]  = 255.255.255.255... <- VERIFY_DONE
            obj2AsObject = [{'ip1': obj2Splitted[0], 'ip2': obj2Splitted[1]}]

        #we will use "isForwardedHere function - determines subsets nicely"
        isFound=False
        obj1AsObject
        obj2AsObject
        for i in obj2AsObject:
            if not (isinstance(i,dict)):
                i = {'ip1':i.ip1, 'ip2':i.ip2}

            for j in obj1AsObject:
                if not (isinstance(j,dict)):
                    j = {'ip1':j.ip1, 'ip2':j.ip2}

                self.logger.debug("Checking %s %s and %s %s" ,j['ip1'], j['ip2'], i['ip1'], i['ip2'])

                if (isForwardedHere(j['ip1'],netmaskToSlash(j['ip2']), i['ip1'], netmaskToSlash(i['ip2']))):
                    isFound = True
                    break
            if(not isFound):
                return False
        #everything found
        return isFound

    def findObject( self, name ):
        """
        Tries to find an object stored (based on name)
        """
        toReturn = []
        for i in self.originalObjects:
            if( i.name == name ):
                self.logger.debug("Found object with name %s in list of original objects",name)
                if( i.__class__.__name__ == "NetObjCisco"):
                    toReturn.append(i)
                elif( i.__class__.__name__ == "NetGroupObjCisco"):
                    for j in i.group:
                        toReturn+=self.findObject(j)
                return toReturn

        for i in self.objectsFromSources:
            if( i.name == name ):
                self.logger.debug("Found object with name %s in list of source objects",name)
                if( i.__class__.__name__ == "NetObjCisco"):
                    toReturn.append(i)
                elif( i.__class__.__name__ == "NetGroupObjCisco"):
                    for j in i.group:
                        toReturn+=self.findObject(j)
                return toReturn

        for i in self.objectsFromDestination:
            if( i[0].name == name ):
                self.logger.debug("Found object with name %s in list of destination objects",name)
                if( i[0].__class__.__name__ == "NetObjCisco"):
                    toReturn.append(i[0])
                elif( i.__class__.__name__ == "NetGroupObjCisco"):
                    for j in i[0].group:
                        toReturn+=self.findObject(j)
                return toReturn

        return ""

    def findService(self, service):
        
        for i in self.services:
            if( i.name == service ):
                #extract info from service
                return i.proto+','+i.number
        #Not found - assuming it is plain service
        return service

    def isServiceSuperSet(self, service1, service2):
        """
        Returns True if the first service (service1) is the super set of the second one
        """
        #early accept
        if(service1 == service2 or service1=="ip"):
            self.logger.debug("Service superset found %s is superset of %s",service1,service2)
            return True
        #Finding sane format of the services:
        serv1Sane = self.findService(service1).split(',')
        serv2Sane = self.findService(service2).split(',')
        if(serv1Sane[0] == serv2Sane[0]):
            if(len(serv1Sane) == 1):
                return True
            elif(len(serv2Sane) == 1):
                return False
            return (serv1Sane[1] == serv2Sane[1])
        return False

    def getFWObjects(self):
        ansibleHandle(self.execDir, "get_asa")
        return 0

    def createSingleNetObj(self, address, zone):
        """
        Creates and returns a single network object from given IP address.
        """
        bookName = ""
        address = address.split('/')
        if len(address) == 1:
           address.append("32")

        tmpNetObj = NetObjCisco( "perunObj"+address[0]+"slash"+address[1],
                                         address[0],
                                         slashToNetmask(int(address[1])),
                                         bookName )
        return tmpNetObj


    def getInterfaceToRout(self, destination ):
        for route in self.routes:

            dstWithSlash = destination.split('/')
            if len(dstWithSlash) == 1:
                dstWithSlash.append(32)
            if (isForwardedHere( route[0],netmaskToSlash(route[1]),dstWithSlash[0],dstWithSlash[1])):
                self.logger.debug("dst :" + destination + " is routed (subnet match) to: " + route[0] + " " + route[1])
                return route[2]
        self.logger.debug("dst :" + destination + " route is not found")
        return ""
    def getZoneOfInterface(self, interface):
        #cisco has interface and zone tightly coupled
        return interface
    def processRuleFile( self, policies ):
        """
        File may look like:
        ["access-group AC2 out interface DMZ", "access-group AC1 out interface inside", "access-group AC3 out interface outside"], ["access-list AC1 extended deny ip any any log ", "access-list AC2 extended deny ip any any ",
 "access-list AC3 extended deny ip any any"]]

        access-list <NAME> extended <permit|deny> <protocol>

        """
        self.logger.debug("Loading Rule file: %s" , policies)
        self.accessGroups = []
        for group in policies[0]:
            parsed_group = group.split(' ')
            name = parsed_group[1]
            direction = parsed_group[2]
            interface = parsed_group[4]
            rules=[]
            skipNext = False
            for policy in policies[1]:

                parsed_policy = policy.split(' ')
                if(skipNext):
                    self.logger.info("Loading of perun rule skipped")

                    if(self.optimizationLevel != 0):
                        self.logger.debug("Deleting from previous run perun rules: %s" , policy)
                        self.to_delete.append("no " + policy)
                    if( (parsed_policy[2] == "remark") and ( parsed_policy[3] =="Perun") ):
                        skipNext = not skipNext
                    continue
                    #skipping remarked perun rule
                acl_name = parsed_policy[1]
                if acl_name == name:
                    self.logger.debug("Loading rules for ACL %s", name)
                    acl_type = parsed_policy[2]
                    if(acl_type == "remark"):
                        self.logger.debug("skipping remark (might be deleted), remarks are not supported yet :( )")
                        if(parsed_policy[3] == "Perun" ):
                            skipNext = not skipNext
                            self.to_delete.append("no " + policy)
                        else:
                            rules.append(RuleCiscoRemark(source=(" ".join(parsed_policy[2:])), destination="", service="", permit="", name=name))
                        continue
                        # skipping load of remark
                    permit = (parsed_policy[3] == "permit")
                    service = parsed_policy[4]
                    parse_counter = 4
                    if (service != 'object') and (service != 'object-group'):
                        parse_counter+=1
                    else:
                        service += " " + parsed_policy[parse_counter+1]
                        parse_counter+=2

                    #source parsing
                    sourceDest = ["",""]
                    sourceDest[0] = parsed_policy[parse_counter]
                    for i in [0,1]:
                        sourceDest[i] = parsed_policy[parse_counter]
                        if sourceDest[i] != "any":
                            if ((sourceDest[i] == 'object') or (sourceDest == 'object-group')):
                                #parse nicely, true no matter what?
                                self.logger.debug("TODO: add objects? or discard") #verify done - handled elsewhere
                            sourceDest[i] +=   " " + parsed_policy[parse_counter+1]
                            parse_counter+=2
                        else:
                            parse_counter+=1
                    if service == "tcp":
                        #handle TCP
                        try:
                            service += ","+" ".join(parsed_policy[parse_counter:])
                        except IndexError:
                            service = "tcp"
                    elif service == "udp":
                        #handle TCP
                        try:
                            service += ","+" ".join(parsed_policy[parse_counter:])
                        except IndexError:
                            service = "udp"

                    self.logger.debug("Rule of %s properties loaded: "
                                 " service: %s , source: %s destination: %s"
                                 " permit: %s"
                                 , name, service, sourceDest[0], sourceDest[1], permit)

                    rules.append(RuleCisco(sourceDest[0],sourceDest[1],service,permit,name))
            self.accessGroups.append(RulePolicyCisco("",interface, rules, name, optimizationLevel=self.optimizationLevel))
        return 0

    def processZoneRouteFile(self, zoneFile, routeFile ):
        
        #strip the info lines about flags
        routeLines=routeFile[0][routeFile[0].index("")+1:]

        for route in routeLines:
            ip=route.split()[1]
            netmask=route.split()[2]
            zone=route.split()[-1]

            self.routes.append((ip,netmask,zone))
            self.logger.debug("Loaded route: dstNET: "+ ip +", dstMASK: " + netmask + ", via zone: " + zone )

        return 0

    def processAddrBook( self, cisco_objects ):
        simple=cisco_objects[0]
        group = cisco_objects[1]
        object=simple.pop(0)
        type = []
        value = []
        while ( simple != [] ):
            line = simple.pop(0)
            if ( line[0] == " " ):
                #Line of object continues here
                localType = line.split()[0]
                localValue = line.split()[1]
                self.logger.debug("local " + localType + " " + localValue)
                if ( localType == "description" ):
                    #skipped for now, shoud be handled before production
                    #value.append(line.split()[1:])
                    continue
                else:
                    type.append(localType)
                    value.append(localValue)
            else:
                self.logger.debug("Loading network object: " + object.split()[2] + ", address/fqdn-or-else: " + value[0])
                #we loaded another object, store previous, continue
                self.originalObjects.append( NetObjCisco(name=object.split()[2], ip1=value[0], ip2="255.255.255.255", bookName="host"))
                object=line
                type = []
                value = []
        if(object != ""):
            self.logger.debug("Loading network object: " + object.split()[2] + ", address/fqdn-or-else: " + value[0])
            self.originalObjects.append( NetObjCisco(name=object.split()[2], ip1=value[0], ip2="255.255.255.255", bookName="host"))

        #manage object groups
        line = group.pop(0)
        while ( group != [] ):
            line = group.pop(0)
            if (line[0]  == " " ):
                # line inside object
                #supports multiple netobjs,
                splitLine=line.split()

                if ( splitLine[1] == "host" ):
                    print("isHost")
                elif (splitLine[1] == "object" ):
                    print("isObject")
                else:
                    print("is raw ip addr+netmask")

        return 0
    def getZonesFromJson( self, zoneFile ):
        #think about deleting this function, in cisco routes are handled with care
        return 0
    def getRoutesFromJson( self, routeFile  ):
        return 2000

    def mergePerunIntoFW( self , perun_rules ):
        #cisco
        perunSourcesNames = [src.name for src in self.objectsFromSources]
        self.logger.info( "perunSourcesNames " + ', '.join( perunSourcesNames ) )

        for i in self.objectsFromDestination:
            print(i[0].name)

        newPols = []
        #accessGroups are sort of policies of the Juniper in Cisco World

        originalAccessGroups = self.accessGroups

        for policy in self.accessGroups:
            print(policy.to_zone)
            lastRule = policy.rules.pop()
            policyNewRulesSplitted = []
            policyNewRulesSplitted.append( policy.rules )

            policyNewRulesSplitted.append([]) #making sure second item exist no matter how many rules will be created


            lineToInsertPerun = len(policy.rules) + 1 + 1
            
            counter = 0
            for rul in perun_rules.keys():
                self.logger.info(rul)
                ip = rul.split(':')[0]
                service = rul.split(':')[1]
                ipWithSlash = ip.split('/')
                if len(ipWithSlash) is 1:
                    ipWithSlash.append("32")
                name = "perunObj" + ipWithSlash[0] + "slash" + ipWithSlash[1]

                int = self.getInterfaceToRout(ipWithSlash[0]+"/"+ipWithSlash[1])
                if int == policy.to_zone:
                    for src in perun_rules[rul]:
                        self.logger.info(src)
                        #sourcesOfThisPol = [k[0] for k in perunSourcesNames if (src in k[0])]
                        sourcesOfThisPol = [k for k in perunSourcesNames if (src in k)]

                        for s in sourcesOfThisPol:
                            self.logger.debug(str(lineToInsertPerun + counter))
                            policyNewRulesSplitted[1].append(RuleCisco(  "object-group "+ s,
                                                            name,
                                                            "perun"+service.split(',')[1] + service.split(',')[0] ,
                                                            True,
                                                            policy.name,
                                                            lineToInsertPerun+counter ))
                            counter += 1
                            #

            policyNewRulesSplitted.append([lastRule])

            policy.rules = policyNewRulesSplitted
            newPols.append(policy)

        self.accessGroups = newPols

    def getRoutInfo():
        return 0
    def getZoneInfo():
        return 0
    def loadPerunObjects():
        return 0
    def setFWObjects():
        return 0
    def deployNetObjs( self ):
        finalList = []
        netObjs =  [i for i in self.objectsFromSources ]

        for i in netObjs:
            finalList += i.print()
            self.logger.debug(i.name)
        for i in self.objectsFromDestination:
            finalList.append(i[0].print())

        self.logger.debug( yaml.dump(finalList) )
        toWrite = { 'perun_processed_net_objs': finalList}


        #TODO: delete objects created from previous runs -> VERIFY_DONE
        
        #output into a file
        with open(self.execDir+'roles/deploy-all-asa/vars/net_objs.yml', 'w') as yaml_file:
            yaml.dump(toWrite,yaml_file, default_flow_style = False)
        return 0
    def deployServices( self ):
        allServices = []
        for i in self.services:
            allServices.append(i.print())
        self.logger.debug( yaml.dump(allServices) )
        toWrite = { 'perun_services': allServices}

        with open(self.execDir+'roles/deploy-all-asa/vars/perun_services.yml', 'w') as yaml_file:
            yaml.dump(toWrite,yaml_file, default_flow_style = False)

        return 0
    def deployPolicies( self ):
        finalList = [[],[]]
        for i in self.accessGroups:
            accessGroup = i.print()
            finalList[0] += accessGroup[0] #access-group
            finalList[1].append(accessGroup[1]) #access-lists
        self.logger.debug(yaml.dump(finalList))
        toWrite = { 'all_policies': list(filter(None, finalList[1])), 'all_groups': list(finalList[0])}

        with open(self.execDir+'roles/deploy-all-asa/vars/rules.yml', 'w') as yaml_file:
            yaml.dump(toWrite,yaml_file, default_flow_style = False)

        toWrite = { 'delete_commands': list(self.to_delete) }
        with open(self.execDir+'roles/deploy-all-asa/vars/to_delete.yml', 'w') as yaml_file:
            yaml.dump(toWrite,yaml_file, default_flow_style = False)
        return 0

    def deployRemoteHard( self ):
        ansibleHandle(self.execDir, tags="deploy_asa")

class FirewallJuniper(Firewall):

    def __init__(self, fwType, execDir , logger, optimizationLevel):
        self.execDir = execDir
        self.type = fwType
        self.logger = logger
        self.optimizationLevel = optimizationLevel
        self.originalObjects = []
        self.originalServices = []

        self.to_delete = []
        if (self.optimizationLevel == 0):
            self.to_delete = ["delete security policies"]

    def getFWObjects(self):
        ansibleHandle(self.execDir, "get_srx")
        return 0

    def getInterfaceToRout(self, destination ):
        dstAsAddrWithMask = destination.split('/')
        if len(dstAsAddrWithMask) == 1:
            dstAsAddrWithMask.append("32")

        for route in self.routes:
            routAsAddrWithMask = route["dst"].split('/')
            if isForwardedHere(routAsAddrWithMask[0], routAsAddrWithMask[1], dstAsAddrWithMask[0], dstAsAddrWithMask[1]):
                return route["nexthop"]
        return ""

    def getZoneOfInterface( self, interface ):
        if interface == "":
            #early deny of the empty interface
            return ""
        for zone in self.zones:
            if interface in zone["interfaces"]:
                return zone["name"]
        return ""

    def getRoutesFromJson(self, jsonRoutes):
        routes = []
        for route in jsonRoutes[0]["rpc-reply"]["route-information"]["route-table"]["rt"]:
            rt_destination = route["rt-destination"]
            submask = int(rt_destination.split('/')[1])
            #store only used routes ( firewall should have stable routes, at least zone-wise)
            priority = ""
            next_hop = ""
            if route["rt-entry"]["active-tag"] is '*':
                priority = route["rt-entry"]["preference"]
                try:
                    if "via" in route["rt-entry"]["nh"].keys():
                        next_hop = route["rt-entry"]["nh"]["via"]
                    elif "nh-local-interface" in route["rt-entry"]["nh"].keys():
                        next_hop = route["rt-entry"]["nh"]["nh-local-interface"]
                except Exception:
                    self.logger.warning("Possibly cannot load some route")
                    continue
            else:
                continue
            routes.append({'dst': rt_destination, 'mask': submask, 'priority':priority, 'nexthop':next_hop })
        #sort by subnet first, priority second, this will enable route lookup
        routesSorted =  sorted( sorted(routes, key=lambda k: k['priority']), key=lambda k: k['mask'], reverse = True)
        routesObjects = []
        self.routes = routesSorted
        return routesSorted

    def getZonesFromJson(self, jsonZone):
        """
        jsonZone is an object contaning json, not the file path
        """
        zones = []
        for zone in jsonZone[0]["rpc-reply"]["zones-information"]["zones-security"]:
            name=""
            interface=[]
            try:
                name = zone["zones-security-zonename"]
                if isinstance( zone['zones-security-interfaces'] , dict):
                    if isinstance( zone['zones-security-interfaces']['zones-security-interface-name'], str ):
                        interface.append(zone['zones-security-interfaces']['zones-security-interface-name'])
                    elif isinstance(zone['zones-security-interfaces']['zones-security-interface-name'], list ):
                        interface = zone['zones-security-interfaces']['zones-security-interface-name']
                elif isinstance( zone['zones-security-interfaces'] , str):
                    interface = []
                pass
            except Exception as e:
                raise
            zones.append({'name':name,'interfaces':interface})
        self.zones=zones
        return zones

    #originally getRules
    def processRuleFile(self, jsonPolicies ):
        """
        Configuration may look like:
            {
        "from-zone-name": "trust",
        "policy": [
            {
                "match": {
                    "application": "any",
                    "destination-address": "any",
                    "source-address": "any"
                },
                "name": "default-permit",
                "then": {
                    "permit": ""
                }
            },
            {
                "match": {
                    "application": "junos-ssh",
                    "destination-address": "perun-addr2",
                    "source-address": "perun-addr1"
                },
                "name": "K",
                "then": {
                    "permit": ""
                }
            }
        ],
        "to-zone-name": "untrust"
        }

        """
        policyList = []
        for policy in jsonPolicies[0]['rpc-reply']['configuration']['security']['policies']['policy']:
            from_zone = policy["from-zone-name"]
            to_zone = policy["to-zone-name"]
            zone_spec_policies=[]
            if isinstance(policy['policy'], dict):
                permit=False
                if 'permit' in policy['policy']['then']:
                    permit=True
                zone_spec_policies.append(RuleJuniper(policy['policy']['match']['source-address'],
                                              policy['policy']['match']['destination-address'],
                                              policy['policy']['match']['application'],
                                              permit,
                                              policy['policy']['name']))

            elif isinstance(policy['policy'], list):
                for pol in policy['policy']:
                    permit=False
                    if 'permit' in pol['then']:
                        permit=True
                    zone_spec_policies.append(RuleJuniper(pol['match']['source-address'],
                                                  pol['match']['destination-address'],
                                                  pol['match']['application'],
                                                  permit,
                                                  pol['name']))
                    #if ("perunRule" in pol['name'] or self.optimizationLevel in [3,4]):
                    if ("perunRule" in pol['name']):
                        self.to_delete.append("delete security policies from-zone " + from_zone + " to-zone " + to_zone + " policy " + pol['name'] )

            policyList.append(RulePolicyJuniper(from_zone,to_zone,zone_spec_policies, optimizationLevel = self.optimizationLevel))
        self.accessGroups = policyList


    def createNetObjectsFromDsts( self, perunDests ):
        counter = 0
        netObjsJuniper = []
        finalZones = []
        for dst in perunDests:
            interface = self.getInterfaceToRout(dst)
            targetZone = self.getZoneOfInterface(interface)
            #FIX: permanent fallback to global zone zonebook
            finalZones.append((dst,targetZone))
            #break;
        # refactor - merge these for cycles
        listOfNetObjs=[]
        for finalZone in finalZones:
            #a = object()
            dstAsAddrWithMask = finalZone[0].split('/')
            if len(dstAsAddrWithMask) == 1:
                dstAsAddrWithMask.append("32")
            #find addrbook based on zone

            bookName = ""
            if finalZone[1] is not '':
                bookName = self.mapZoneBook[finalZone[1]]
            if bookName is '':
                bookName="global"
            tmpNetObj = NetObjJuniper( "perunObj"+dstAsAddrWithMask[0]+"slash"+dstAsAddrWithMask[1],
                                        dstAsAddrWithMask[0],
                                        slashToNetmask(int(dstAsAddrWithMask[1])),
                                        bookName )
            #we need to keep track of the zone - adrbook is not enough
            listOfNetObjs.append((tmpNetObj, finalZone[1]))
        self.objectsFromDestination = listOfNetObjs

    def createNetObjectsFromSource (self, perunSrcs , srcNames ):

        sourcesAsObjects = []
        tuplesZoneSources = []
        for source in perunSrcs:
            #perunSrcs is a list of lists, we need to determine whether every object of inner list is from the same zone
            helpArrayOfZones=[]
            for item in source:
                interface = self.getInterfaceToRout(item)
                zone = self.getZoneOfInterface(interface)
                helpArrayOfZones.append(zone)
            #sort src (list of sources) according to values of helpArrayOfZones
            #then split into subs
            if(helpArrayOfZones[1:] is helpArrayOfZones[:-1]):
                #prob. could be removed, need to be tested
                tuplesZoneSources.append( source, helpArrayOfZones.pop() )
                continue
            src = [x for _,x in sorted(zip(helpArrayOfZones,source))]
            helpArrayOfZones = sorted(helpArrayOfZones)
            masterArray = []
            whereStuffChange = [ i for i, (x, y) in enumerate(zip(helpArrayOfZones[:-1],helpArrayOfZones[1:])) if x!=y]
            whereStuffChange.reverse()

            for index in whereStuffChange:
                subList = source[index+1:]
                zone = helpArrayOfZones.pop()
                masterArray.append((subList,zone))
                helpArrayOfZones = helpArrayOfZones[:index+1]
                source=source[:index+1]

            #append last item
            masterArray.append((source,helpArrayOfZones.pop()))

            tuplesZoneSources.append(masterArray)
        tuplesZoneSourcesNames=dict(zip(srcNames, tuplesZoneSources))
        #create NetObj
        allGroups = []
        for name in tuplesZoneSourcesNames.keys():
            i = tuplesZoneSourcesNames[name]

            if type(i) is tuple:
                #create a single address-set
                #j is a single addr inside list inside tuple
                groupAsList = []
                for j in i[0]:
                    #find a bookName
                    singleAdrAsObj = self.createSingleNetObj(j ,i[1])
                    groupAsList.append(singleAdrAsObj)
                allGroups.append(groupAsList)
            elif type(i) is list:

                counter = 0
                #create multiple address sets
                for p in i:
                    if(p[1] == ''):
                        self.logger.warning("Some of the sources cannot be routed correctly %s", ' '.join(p[0]))
                        break
                    groupAsList = []
                    for j in p[0]:
                        #find a bookName
                        singleAdrAsObj = self.createSingleNetObj(j, p[1])
                        groupAsList.append( singleAdrAsObj )
                    #
                    allGroups.append( NetGroupObjJuniper( name +"-" +str(counter), groupAsList, self.mapZoneBook[p[1]] ))
                    counter+=1
        self.objectsFromSources = allGroups


    def createSingleNetObj(self, address, zone):
        """
        here comes..
        """
        bookName = ""
        address = address.split('/')
        if len(address) == 1:
           address.append("32")
        #find addrbook based on zone

        bookName = ""
        if zone is not '':
            bookName = self.mapZoneBook[zone]
        if bookName is '':
            bookName="global"
        tmpNetObj = NetObjJuniper( "perunObj"+address[0]+"slash"+address[1],
                                         address[0],
                                         slashToNetmask(int(address[1])),
                                         bookName )
        return tmpNetObj

    def createServices( self, servToDeploy ):
        servicesDeployed=[]
        for service in servToDeploy:
            s=service.split(',')
            toReturn = object()
            if s[1] is "icmp":
               toReturn = IcmpObjJuniper('perun'+s[1]+s[0],s[1],s[0])
            else:
               toReturn = ServiceObjJuniper('perun'+s[1]+s[0], s[1], s[0])
            servicesDeployed.append(toReturn)
        self.services = servicesDeployed

    def loadPerunObjects():
        return 0
    def mergePerunIntoFW( self , perun_rules ):

        perunSourcesNames = []
        for i in self.objectsFromSources:
            perunSourcesNames.append((i.name,  next(key for key, value in self.mapZoneBook.items() if value == i.bookname ) ))
            for j in i.group:
                print(j.print())
        print(perunSourcesNames)

        newPols = []
        counter = 0
        for policy in self.accessGroups:
            newOriginalAndPerun = []
            lastRule = policy.rules.pop()
            #we need to find perun rules, with destinations
            print(policy.to_zone)
            print(policy.from_zone)

            newOriginalAndPerun.append(policy.rules)

            newOriginalAndPerun.append([])

            for rul in perun_rules.keys():

                ip = rul.split(':')[0]
                service = rul.split(':')[1]
                #find perunDestination with ip
                ipWithSlash = ip.split('/')
                if len(ipWithSlash) is 1:
                    ipWithSlash.append("32")
                name = "perunObj" + ipWithSlash[0] + "slash" + ipWithSlash[1]

                int = self.getInterfaceToRout(ipWithSlash[0]+"/"+ipWithSlash[1])
                zone = self.getZoneOfInterface(int)

                if zone == policy.to_zone:
                    for src in perun_rules[rul]:
                        sourcesOfThisPol = [k[0] for k in perunSourcesNames if (src in k[0]) and (policy.from_zone == k[1])]

                        for s in sourcesOfThisPol:
                            newOriginalAndPerun[1].append(  RuleJuniper(s,
                                                            name,
                                                            "perun"+service.split(',')[1] + service.split(',')[0] ,
                                                            True,
                                                            "perunRule"+str(counter),
                                                            lastRule.name) )
                        counter += 1
            newOriginalAndPerun.append([lastRule])
            policy.rules = newOriginalAndPerun
            newPols.append(policy)

        for pol in newPols:
            print(pol.print())

        #print("Born to be sad.")

        #test if it is secure (changed 1.4.2019 - the following line)
        self.rules = newPols

        return newPols

    def processAddrBook( self, juniper_books ):
        """
        "rpc-reply" | .configuration | .security | ."address-book"
        """
        mapZoneBook = {}
        for adrBook in juniper_books[0]["rpc-reply"]["configuration"]["security"]["address-book"]:
            #security zone must be unique in addr book
            if isinstance(adrBook,str):
                #Just a single address book used - most likely a global one
                self.logger.warning("Cannot load a single book")
                break

            if "attach" in adrBook.keys():
                mapZoneBook[adrBook["attach"]["zone"]["name"]] = adrBook["name"]
                if "address" in adrBook.keys():
                    if isinstance(adrBook["address"], dict):
                        object=adrBook["address"]
                        self.originalObjects.append(NetObjJuniper(name=object["name"],
                                                                    ip1=object['ip-prefix'].split('/'),
                                                                    ip2 = object['ip-prefix'].split('/'),
                                                                    bookName=adrBook['name']))
                    elif isinstance(adrBook["address"],list):
                        for object in adrBook["address"]:
                            self.originalObjects.append(NetObjJuniper(name=object["name"],
                                                                      ip1=object['ip-prefix'].split('/'),
                                                                      ip2 = object['ip-prefix'].split('/'),
                                                                      bookName=adrBook['name']))
                if "address-set" in adrBook.keys():
                    for objectGroup in adrBook["address-set"]:
                        print(objectGroup)
                        if (isinstance(objectGroup["address"], list)):
                            self.originalObjects.append(NetGroupObjJuniper(name=objectGroup["name"],
                                                                           group=[i['name'] for i in objectGroup['address'] ],
                                                                           bookname=adrBook["name"]))
                        elif( isinstance(objectGroup["address"], dict)):
                            self.originalObjects.append(NetGroupObjJuniper(name=objectGroup["name"],
                                                                           group=[ objectGroup['address']['name'] ],
                                                                           bookname=adrBook["name"]))
        for service in juniper_books[0]["rpc-reply"]["configuration"]["applications"]["application"]:
            if (isinstance(service,str)):
                #there is only a single one in the config (not a list)
                app = juniper_books[0]["rpc-reply"]["configuration"]["applications"]["application"]
                self.originalServices.append(ServiceObjJuniper(name=app["name"],proto=app["protocol"],number=app["destination-port"]))
                break
            self.originalServices.append(ServiceObjJuniper(name=service["name"],proto=service["protocol"],number=service["destination-port"]))

        self.mapZoneBook = mapZoneBook

    def processZoneRouteFile(self, zoneFile, routeFile ):
        zone = self.getZonesFromJson( zoneFile  )
        interfaceRoutes = self.getRoutesFromJson( routeFile )
        self.zonesAndRoutes = {'zones': zone, 'routes': interfaceRoutes}

    def processJuniperRuleFile( rule_file ):
        with open( rule_file , "r" ) as source:
            juniper_policies = json.load( source )
        pols = getRulesFromJsonJunos( juniper_policies )
        self.rules = pols

    def setFWObjects():
        return 0
    def deployPolicies():
        return 0
    def deployNetObjs( self ):
        finalList = []
        netObjs = [ i[0] for i in self.objectsFromDestination ] + [i for i in self.objectsFromSources ]

        for i in netObjs:
            finalList.extend(i.print().split('\n'))
        toWrite = { 'perun_processed_net_objs': list(set(filter(None, finalList)))}
        #TODO: output into a file -> DONE
        with open(self.execDir+'roles/deploy-all-srx/vars/net_objs.yml', 'w') as yaml_file:
            yaml.dump(toWrite,yaml_file, default_flow_style = False)

    def deployServices( self ):
        with open(self.execDir+'roles/deploy-all-srx/vars/perun_services.yml', 'w') as yaml_file:
            yaml.dump({ 'perun_services': [i.print() for i in self.services]},
                      yaml_file,
                      default_flow_style = False)


    def deployPolicies( self ):
        finalList = []
        for i in self.accessGroups:
            finalList.extend(i.print().split('\n'))
        toWrite = { 'all_policies': list(filter(None, finalList))}
        with open(self.execDir+'roles/deploy-all-srx/vars/rules.yml', 'w') as yaml_file:
            yaml.dump(toWrite,yaml_file, default_flow_style = False)

        toDelete = { 'delete_policies': self.to_delete}
        with open(self.execDir+'roles/deploy-all-srx/vars/to_delete.yml', 'w') as yaml_file:
            yaml.dump(toDelete,yaml_file, default_flow_style = False)

    def deployRemoteHard( self ):
        ansibleHandle(self.execDir, tags="deploy_srx")

    def deployRemote( self , perunRules ):
        self.mergePerunIntoFW( perunRules )
        #rename deploy to prepare files to deploy to make it more accurate
        self.optimizeRules()
        self.deployNetObjs()
        self.deployServices()
        self.deployPolicies()
        self.deployRemoteHard()
        return 0

    def isShadowed( self, rule1, rule2):
        """
        Return True if the second rule is shadowed by the first one.
        """
        if(rule1.name == rule2.name):
            return False
        print(  rule1.source, rule2.source,
                          rule1.destination, rule2.destination,
                          rule1.service, rule2.service)
        self.logger.debug("Check Shadowing - Sources: %s and %s \n"
                          "Destinations %s and %s \n"
                          "Services %s and %s"
                          , rule1.source, rule2.source,
                          rule1.destination, rule2.destination,
                          rule1.service, rule2.service
                          )
        if(rule1.permit != rule2.permit):
            self.logger.info("Rules does match their deny/permit property, possibly correlation - Check manually ")
            self.logger.info("(possible correlation) Check manually: - Sources: %s and %s \n"
                          "Destinations %s and %s \n"
                          "Services %s and %s"
                          , rule1.source, rule2.source,
                          rule1.destination, rule2.destination,
                          rule1.service, rule2.service
                          )
            #possible TODO: check whether the rules overlaps/shadows
            return False
        if( self.isObjSuperSet( rule1.source, rule2.source)           and
            self.isObjSuperSet( rule1.destination, rule2.destination) and
            self.isServiceSuperSet( rule1.service, rule2.service)     ):
            self.logger.info("Rule is shadowed by another, and will be deleted:\n"
                              "%s \n"
                              "shadowed: %s\n",rule1.print(),rule2.print())

            return True

        return False
    def isObjSuperSet(self, obj1, obj2 ):
        """
        Returns True if the first object (obj1) is the super set of the second one
        """
        isFound = False
        if(obj1 == "any"):
            return True
        else:
           obj1AsIps = self.findObject(obj1)
           obj2AsIps = self.findObject(obj2)

        for i in obj2AsIps:
            isFound = False
            for j in obj1AsIps:
                self.logger.debug("Checking %s %s " , j, i)
                if ( isForwardedHere( j[0],j[1], i[0], i[1] ) ):
                    isFound = True
                    break
            if(not isFound):
                return False
        #everything found
        return isFound

    def findObject( self, name ):
        """
        Tries to find an object stored (based on name)
        """
        toReturn = ""

        dests = [ i[0] for i in self.objectsFromDestination]

        for i in self.originalObjects + self.objectsFromSources + dests:
            if( i.name == name ):
                self.logger.debug("Found object with name %s in list of objects",name)
                toReturn = i
                break;

        toReturnGroup = []
        if(toReturn.__class__.__name__ == 'NetGroupObjJuniper'):
            for i in toReturn.group:
                if (isinstance(i,NetObjJuniper)):
                    if(isinstance(i.ip1,list)):
                        toReturnGroup += [i.ip1]
                    else:
                        toReturnGroup += [[i.ip1,netmaskToSlash(i.ip2)]]
                elif (isinstance(i,str)):
                    toReturnGroup += self.findObject(i)
            return toReturnGroup

        if toReturn != "":
            if(isinstance(toReturn.ip1,list)):
                print(toReturn.ip1)
                return [toReturn.ip1]
            else:
                print("notlist")
                print(toReturn.ip1 + toReturn.ip2)
                return [[toReturn.ip1,netmaskToSlash(toReturn.ip2)]]
        else:
            self.logger.debug("Item not found.")
            return ""


    def findService(self, service):
        
        for i in self.services:
            if( i.name == service ):
                #extract info from service
                return i.proto+','+i.number
        #Not found - assuming it is plain service
        return service

    def isServiceSuperSet(self, service1, service2):
        """
        Returns True if the first service (service1) is the super set of the second one
        """
        #early accept
        if(service1 == service2 or service1=="any"):
            self.logger.debug("Service superset found %s is superset of %s",service1,service2)
            return True
        #Finding sane format of the services:
        serv1Sane = self.findService(service1).split(',')
        serv2Sane = self.findService(service2).split(',')
        if(serv1Sane[0] == serv2Sane[0]):
            if(len(serv1Sane) == 1):
                return True
            elif(len(serv2Sane) == 1):
                return False
            return (serv1Sane[1] == serv2Sane[1])
        #NotImplementedYet
        return False

    #END OF JUNOS

class FirewallOpenStack(Firewall):
    #OSTACK
    def mergePerunIntoFW( self, rules ):
        #NotImplementedYet
        #raise NotImplementedError
        self.accessGroups=[]
        print(rules)
        for i in rules.keys():
            ipAddr = i.split(':')[0]
            serv = i.split(':')[1]
            for j in self.routes.keys():
                finalList = []
                for routIP in self.routes[j]:
                    ipAddrWithNet = ipAddr.split('/')
                    if(len(ipAddrWithNet) == 1):
                        ipAddrWithNet.append('32')

                    if (isForwardedHere(routIP, 32, ipAddrWithNet[0], int(ipAddrWithNet[1]))):
                        print("FOUND")
                        for source in rules[i]:
                            for srcIP in self.sources[source]:
                                finalList.append(RuleOStack(source=srcIP,destination="", service=serv, permit=True, name=""))
                if(finalList != []):
                    self.accessGroups.append(RulePolicyOStack(from_zone="",to_zone="", rules=[[],finalList,[]] , name = j, optimizationLevel = 1))

        return 0
    def getFWObjects(self):
        ansibleHandle(self.execDir, 'get_ostack')

    def processRuleFile(self, file):
        return 0

    def getRoutesFromJson(self, jsonRoutes):
        #NotImplementedYet
        print(jsonRoutes)
        self.routes=[]
        allSecurities = {}

        for server in jsonRoutes['ansible_facts']['openstack_servers']:
            srvAddresses = []
            for network in server['addresses']:
                for address in server['addresses'][network]:
                    #print(address+network)
                    if ('addr' in address.keys()):
                        srvAddresses.append(address['addr'])

            #adding to first only
            sec_group = server['security_groups'][0]["name"]
            if not (sec_group in allSecurities.keys()):
                allSecurities[sec_group] = srvAddresses
            else:
                allSecurities[sec_group] += srvAddresses

        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        self.routes = allSecurities
        print(self.routes)

        return 0

    def getRoutInfo(self):
        return 0

    def createServices( self, servToDeploy ):
        """OpenStack - no support for objects - nothing to do
        """
        return 0

    def createNetObjectsFromDsts( self, perunDests ):
        """OpenStack - no support for objects - nothing to do
        """
        return 0
    def createNetObjectsFromSource (self, perunSrcs , srcNames ):
        """OpenStack - no support for objects - nothing to do
        """
        print(perunSrcs)
        print(srcNames)
        self.sources = dict(zip(srcNames,perunSrcs))

        print(self.sources)

        return 0
    def processZoneRouteFile( self, file1,  file2):
        self.getRoutesFromJson(file2)
        return 0
    def processAddrBook( self, file1 ):
        return 0


    def getZoneInfo(self):
        return 0
    def loadPerunObjects(self):
        return 0
    def setFWObjects(self):
        return 0
    def deployNetObjs(self):
        return 0
    def deployServices(self):
        return 0
    def deployPolicies(self):

        toWrite=[]
        for i in self.accessGroups:
            toWrite.extend(i.print())
        print( yaml.dump(toWrite, default_flow_style = False ) )

        with open(self.execDir+'roles/deploy-all-ostack/vars/rules.yml', 'w') as yaml_file:
            yaml.dump({'policies': toWrite},yaml_file, default_flow_style = False)

        return 0

    def deployRemoteHard(self):
        ansibleHandle(self.execDir, tags="deploy_ostack")

    def deployRemote( self , perunRules ):
        self.mergePerunIntoFW( perunRules )
        #self.optimizeRules()
        #self.deployNetObjs()
        #self.deployServices()
        self.deployPolicies()
        self.deployRemoteHard()
        return 0

    def optimizeRules( self ):
        return 0

    def optimizeSetFromOther( self, ruleset1, ruleset2 ):
        return 0

    def optimizeSingleSet( self, ruleset ):
        return 0

    def isRedundant( self, rule1, rule2 ):
        return 0

class GenericObj(object):
    """docstring for NetObj."""
    def __init__(self, arg):
        super(NetObj, self).__init__()
        self.arg = arg

    # - --------------------- interface -------------

class NetGroupObj(object):
    """docstring for NetGroupObj."""
    def __init__(self, name, group , bookname="global"):
        super(NetGroupObj, self).__init__()
        self.arg = arg

    def print(self):
        return ""


class ServiceGroupObj(object):
    """docstring forServiceGroupObj."""
    def __init__(self, arg):
        super(ServiceGroupObj, self).__init__()
        self.arg = arg

    def print(self):
        return ""

class IcmpGroupObj(object):
    """docstring for IcmpGroupObj."""
    def __init__(self, arg):
        super(IcmpGroupObj, self).__init__()
        self.arg = arg

    def print(self):
        return ""

class HostGroupObj(object):
    """docstring for HostGroupObj."""
    def __init__(self, arg):
        super(HostGroupObj, self).__init__()
        self.arg = arg

    def print(self):
        return ""


class NetObj(object):
    def __init__(self, name, ip1, ip2, bookName = None):
        super(NetObj,self).__init__()

    def print(self):
        return ""


class ServiceObj(object):
    """docstring forServiceObj."""
    def __init__(self, arg):
        raise NotImplementedYet

    def print(self):
        return ""

class IcmpObj(object):
    """docstring for IcmpObj."""
    def __init__(self, arg):
        super(IcmpObj, self).__init__()
        self.arg = arg

    def print(self):
        return ""

class HostObj(object):
    """docstring for HostObj."""
    def __init__(self, type, addr1, addr2, description):
        super(HostObj, self).__init__()
        self.arg = arg

    def print(self):
        return ""

# --------------------- impementation --------------------------


class RulePolicy(object):
    """docstring RulePolicy is a set of Rules - has from or to policy, in Cisco world one is more than enough (in case of MU only to is implemented), in case of juniper both are neccessary"""
    def __init__(self, from_zone, to_zone, rules, name = "Abrakadabra" , optimizationLevel = 1):
        self.from_zone = from_zone
        self.to_zone = to_zone
        self.rules = rules
        self.name = name
        self.optimizationLevel = optimizationLevel


class Rule(object):
    """docstring for Rule."""
    def __init__(self, source, destination, service, permit, name, position = 0):
        super(Rule, self).__init__()
        self.source = source
        self.destination = destination
        self.service = service
        self.permit = permit
        self.name = name
        self.position = position
    def print(self):
        raise NotIm


class NetObj(object):
    """docstring for NetObj.
      in Cisco        -> (config) # object network obj_name
                         (config-network-object)# {host ip_addr | subnet net_addr net_mask |
range ip_addr_1 ip_addr_2}
      in Juniper      ->

    """
    def __init__(self, name, ip1, ip2, bookName):
        super(NetObj, self).__init__()
        self.name = name
        self.ip1 = ip1
        self.ip2 = ip2
        if(ip2 == "32"):
            self.typeNet = host
        self.bookName = bookName
    def printCisco():
        """
        - asa_config:
            lines:
              - network-object host 10.80.30.18
              - network-object host 10.80.30.19
              - network-object host 10.80.30.20
            parents: ['object-group network OG-MONITORED-SERVERS']

        would be: [{'asa_config': {'lines': ['network-object host 10.80.30.18', 'network-object host 10.80.30.19', 'network-object host 10.80.30.20'], 'parents': ['object-group network OG-MONITORED-SERVERS']}}]

        hostname(config)# object-network OBJECT1
        hostname(config-network-object)# host 10.2.2.2
        [{'asa_config': {'lines': ['host 10.2.2.2'], 'parents': ['object-network OBJECT1']}}]
        """
        if(self.typeNet == 3):
            return  [{'asa_config': {'lines': ['host ' + self.ip1 ], 'parents': ['object-network ' + self.name ]}}]
        elif(self.typeNet == 2):
            return [{'asa_config': {'lines': ['range ' + self.ip1 + ' ' + self.ip2 ], 'parents': ['object-network ' + self.name ]}}]
        else:
            return [{'asa_config': {'lines': ['subnet ' + self.ip1 + ' ' + self.ip2 ], 'parents': ['object-network ' + self.name ]}}]

    def printJuniper():
        """
        TODO:
        1. Find out which module to use (juniper_junos_command or juniper_junos_config)
        2. create a more smooth way to deploy (deleting everything but lines themsel)

        """
        if(self.typeNet == 3):
            return [{'junos_config': {'lines': ['set address-book ' + self.bookName + ' address ' + self.name + ' ' + self.ip1 ]}}]

class ServiceObj(object):
    """docstring forServiceObj."""
    def __init__(self, name, proto, number):
        self.name = name
        self.proto = proto
        self.number = number

    def print():
        raise NotImplementedError


class HostObj(object):
    """docstring for HostObj."""
    def __init__(self, type, addr1, addr2, description):
        super(HostObj, self).__init__()
        self.arg = arg

class NetGroupObj(object):
    """docstring for NetGroupObj."""
    def __init__(self, name, group , bookname="global"):
        super(NetGroupObj, self).__init__()
        self.name = name
        self.group = group
        self.bookname = bookname

class ServiceGroupObj(object):
    """docstring forServiceGroupObj."""
    def __init__(self, arg):
        super(ServiceGroupObj, self).__init__()
        self.arg = arg

class HostGroupObj(object):
    """docstring for HostGroupObj."""
    def __init__(self, arg):
        super(HostGroupObj, self).__init__()
        self.arg = arg

        # ----------------- Juniper ------------------
class RulePolicyJuniper(RulePolicy):
    """docstring RulePolicy is a set of Rules - has from or to policy, in Cisco world one is more than enough (in case of MU only to is implemented), in case of juniper both are neccessary"""
    def print(self):
        toReturn = ""

        print(self.rules[2][0].print())
        if (self.optimizationLevel == 0):
            print("Just a statement")
            iterate = self.rules[0] + self.rules[2] + self.rules[1]
        elif (self.optimizationLevel in [3,4] ):
            iterate = self.rules[0] + self.rules[1]
        else:
            iterate = self.rules[1]

        #for pol in self.rules
        for pol in iterate:
            k = pol.print().split('\n')
            toReturn+="set security policies from-zone " + self.from_zone + " to-zone " + self.to_zone + k[0] + "\n"
            toReturn+="set security policies from-zone " + self.from_zone + " to-zone " + self.to_zone + k[1] + "\n"
            if(len(k)>2):
                toReturn+="insert security policies from-zone " + self.from_zone + " to-zone " + self.to_zone + k[2]+"\n"
            """ set security policies from-zone trust to-zone untrust """


        return toReturn

class RulePolicyCisco(RulePolicy):
    def print(self):
        toReturn = [[],[]]
        if self.from_zone == "":
            toReturn[0].append("access-group " + self.name + " out interface " + self.to_zone)
        elif self.to_zone == "":
            toReturn[0].append("access-group " + self.name +  " in interface " + self.from_zone)
        if (self.optimizationLevel == 0):
            rulesToPrint = self.rules[0] + self.rules[1] + self.rules[2]
        else:
            rulesToPrint = self.rules[1]
        if(self.optimizationLevel in [0,3,4] ):
            for pol in self.rules[0]:
                printedPolicy = pol.print()
                for x in printedPolicy.split('\n'):
                    toReturn[1].append(x)

        start_line = len(self.rules[0]) + 1
        #if(start_line > 1):
        #    start_line-=1

        end_line = start_line
        toReturn[1].append("access-list " + self.name + " line " + str(start_line) + " remark Perun generated rules will follow")
        for pol in self.rules[1]:
            end_line = pol.position # Not the pole position :)
            printedPolicy = pol.print()
            for x in printedPolicy.split('\n'):
                toReturn[1].append(x)
        toReturn[1].append("access-list " + self.name + " line " + str(end_line + 1) + " remark Perun generated rules end")

        if(self.optimizationLevel == 0):
            for pol in self.rules[2]:
                printedPolicy = pol.print()
                for x in printedPolicy.split('\n'):
                    toReturn[1].append(x)

        return toReturn

class RulePolicyOStack( RulePolicy ):
    """ Docstring for RulePolicyOStack
    """
    def print( self ):
        toReturn = []
        for i in self.rules[1]:
            tempVar = i.print()
            tempVar['security_group'] = self.name
            toReturn.append(tempVar)
        return toReturn

class RuleJuniper(Rule):
    """docstring for Rule."""
    """  set security policies from-zone trust to-zone untrust policy L match source-address l destination-address dl application any """
    """  set security policies from-zone trust to-zone untrust policy L then permit """
    def print(self):
        if(isinstance(self.service,list)):
            toReturn = " policy " + self.name + " match source-address " + self.source + " destination-address " + self.destination + " application [ " + ' '.join(self.service) +" ]"
        else:
            toReturn = " policy " + self.name + " match source-address " + self.source + " destination-address " + self.destination + " application " + self.service
        toReturn += '\n'
        if self.permit:
            toReturn += " policy " + self.name + " then permit"
        else:
            toReturn += " policy " + self.name + " then deny"
        if (self.position != 0):
            toReturn += '\n'
            toReturn += " policy " +self.name + " before policy " + self.position

        return toReturn

class RuleCisco(Rule):
    """access-control entry for cisco ASA, the objects are not directly stored, only an identificator is present."""
    def print(self):
        toReturn = ""
        line = ""
        if(self.position > 0):
            line = " line "+ str(self.position)

        #toReturn += '\n'
        #if any( "perun" in x for x in [self.service, self.destination, self.source]):
        #    toReturn += "access-list "+ self.name + line +" remark perun generated rule will follow\n"
        #return self.source
        srvice = []
        if( (self.service == "any")  or (self.service == "ip") ):
            srvice = [self.service]
        elif( len(self.service.split(',') ) == 2 ):
            srvice =  self.service.split(',') #the other side of tcp/udp such as eq port should be after comma
        else:
            #lets say it is just object, services such as TCP with port spec has to be handled here
            if( len(self.service.split(' ')) == 2 ):
                srvice.append(self.service)
                #example of this occur on the tcp service without the ports specified
            else:
                srvice = ["object " + self.service]

        if( (self.destination == "any")   ):
            dest = self.destination
        elif( len(self.destination.split(' ') ) == 2 ):
            if(self.destination.split(' ')[0] == "object-group"):
                dest = self.destination
            elif(self.destination.split(' ')[0] == "object"):
                dest = self.destination
            else:
                dest =  self.destination
        else:
            #lets say it is just object, it needs to be found and verified it is surely not
            dest = "object " + self.destination

        if( (self.source == "any") ):
            sourc = self.source
        elif( len(self.source.split(' ')) == 2 ):
            if( self.source.split(' ')[0] == "object-group" ):
                sourc = self.source
            elif(self.source.split(' ')[0] == "object"):
                sourc = self.source
            else:
                sourc =  self.source
        else:
            #lets say it is just object, it needs to be found and verified
            sourc = "object " + self.source

        if self.permit:
            toReturn += "access-list " + self.name + line + " extended permit " + srvice[0] + " " + sourc + " " + dest
        else:
            toReturn += "access-list " + self.name + line +" extended deny " + srvice[0] + " " + sourc + " " + dest
        if (len(srvice) == 2):
            toReturn +=  " " + srvice[1]
        return toReturn

class RuleOStack( Rule ):
    def print( self ):
        toReturn = {}
        toReturn['remote_ip_prefix'] = self.source
        protocol = self.service.split(',')
        toReturn['protocol'] = protocol[0]
        if (len(protocol) > 1 ):
            toReturn['port_range_min'] = protocol[0]
            toReturn['port_range_max'] = protocol[0]
            toReturn['protocol'] = protocol[1]
        return toReturn

class RuleCiscoRemark(Rule):
    def print( self ):
        line = ""
        if(self.position > 0):
            line = " line " + self.position
        return "access-list " + self.name + line + " remark "  + self.source

class NetObjJuniper(NetObj):
    """docstring for NetObj.
      in Cisco        -> (config) # object network obj_name
                         (config-network-object)# {host ip_addr | subnet net_addr net_mask |
range ip_addr_1 ip_addr_2}
      in Juniper      ->

    """
    def print(self):
        """
        TODO:
        1. Find out which module to use (juniper_junos_command or juniper_junos_config)
        """
        toReturn = 'set security address-book ' + self.bookName + ' address ' + self.name + ' ' + self.ip1
        return toReturn

class ServiceObjJuniper(ServiceObj):
    """docstring forServiceObj."""
    def print(self):
        return "set applications application "+ self.name + " protocol "+ self.proto + " destination-port " + self.number

class IcmpObjJuniper(ServiceObj):
    """docstring for IcmpObj."""
    def print(self):
        return "set applications application " + self.name + " icmp-type " + self.number

class NetGroupObjJuniper(NetGroupObj):
    """docstring for NetGroupObj."""
    def print(self):

        toReturn = ""
        for i in self.group:
            if type(i) is NetGroupObjJuniper:
                toReturn += 'set security address-book ' + self.bookname + ' address-set ' + self.name + ' address-set ' + i.name + '\n'
            else:
                toReturn += 'set security address-book ' + self.bookname + ' address-set ' + self.name + ' address ' + i.name + '\n'
            #fix redundancy
            toReturn+= i.print() + '\n'
        return  toReturn


class NetObjCisco(NetObj):
    """docstring for NetObj.
      in Cisco        -> (config) # object network obj_name
                         (config-network-object)# {host ip_addr | subnet net_addr net_mask |
range ip_addr_1 ip_addr_2}
      in Cisco      ->

    """
    def print(self):
        """
        TODO:
        1. Find out which module to use (juniper_junos_command or juniper_junos_config)
        2. create a more smooth way to deploy (deleting everything but lines themsel)
        to achieve:
        object network OBJ8
          host 10.1.1.105
        """
        print("debug statement")
        toReturn = { 'parents': [ 'object network ' + self.name ], 'lines':[ 'host '+self.ip1 ] }
        return toReturn

class ServiceObjCisco(ServiceObj):
    """docstring forServiceObj."""
    def print(self):
        object = { 'parents': ['object service ' + self.name ], 'lines': ['service ' + self.proto + ' destination eq ' + self.number] }
        return object

class NetGroupObjCisco(NetGroupObj):
    """docstring for NetGroupObj."""

    def print( self ):
        toReturn = []
        lines = []
        for i in self.group:
            lines.append('network-object object ' + i.name)
            if (type(i) is NetGroupObjCisco):
                #notsupportedyet
                pass
            elif (type(i) is NetObjCisco):
                toReturn.append(i.print())
        toReturn.append({'parents': ['object-group network ' + self.name ], 'lines': lines})

        return toReturn

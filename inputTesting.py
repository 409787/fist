{
   "rules" : [
      {
         "allowedIPs" : [
            "10.1.1.10",
            "10.1.1.2",
            "10.1.2.3",
            "10.1.2.4"
         ],
         "rule" : "147.251.22.212:22,tcp"
      },
      {
         "allowedIPs" : [
            "10.1.1.10",
            "10.1.1.2",
            "10.1.2.3",
            "10.1.2.4"
         ],
         "rule" : "147.251.22.214:8082,tcp"
      },
      {
         "allowedIPs" : [
            "10.1.1.2",
            "10.1.1.5",
            "10.1.2.2"
         ],
         "rule" : "147.251.54.223:22,tcp"
      },
      {
         "allowedIPs" : [
            "10.1.1.10",
            "10.1.1.2",
            "10.1.1.5",
            "10.1.2.2",
            "10.1.2.3",
            "10.1.2.4"
         ],
         "rule" : "147.251.54.225:6633,tcp"
      },
      {
         "allowedIPs" : [
            "190.1.1.10",
            "170.1.1.2",
            "10.1.1.5",
            "10.1.2.2",
            "102.1.2.3",
            "10.1.2.4"
         ],
         "rule" : "147.250.1.225:22,tcp"
      }
   ]
}

#!/usr/bin/python
#import firewallRules as fw
import json
import argparse
import fist_objects as objs
import os
import yaml
import logging
import logging.handlers

#import string

#
def pidof(name):
    output=[]
    os.system( "ps -aef | grep '"+ name +"' | grep -v 'grep' | awk '{ print $2 }' > /tmp/pidofile")
    with open('/tmp/pidofile', 'r') as f:
        line = f.readline()
        while line:
            output.append(line.strip())
            line = f.readline()
            if line.strip():
                output.append(line.strip())
    return output
#-----Helper functions -------
# providing some of the computational # ------ End of DEPRECATED ------
class CommunicationError(Exception):
    """Exception raised for errors in communication

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message



#------------------Ansible playbooks ----------------------------
# TODO: remove this and use just the one in the fist_objects
def ansibleHandle( directory, tags):
    """Run ansible-playbook with specified tags, return the return value.

    directory -- the root directory of the playbooks - where manage-fw.yml lives
    tags -- which tags should be run, see manage-fw.yml to determine This
    """
    return os.system('ansible-playbook -i '+directory+'../ansible-setting/ansible_inventory --private-key '+directory+'../ansible-setting/private_key  '+directory+'manage-fw.yml -t '+ tags)

# ----- FW processing
def getFirewallType( directory, logger ):
    """Try to communicate with the FW, requires to have prepared files in
    order to determine how to connect to the FW
    """
    #determine the firewall type based on a ansible show version playbook call
    logger.info("Determining the firewall type...")
    #if os.system('ansible-playbook -i ../ansible-setting/vagrant_ansible_inventory --private-key ../ansible-setting/private_key -u vagrant manage-fw.yml -t version_asa') == 0:

    #Junos has to be checked first - ASA playbook can connect to the Juniper SRX just the command "show version" will fail, but - that means that whole playbook will not fail - will return the zero code
    if ansibleHandle( directory, "version_srx" ) == 0:
        #return that the appliance is Juniper SRX
        logger.info("Success we found SRX")
        return "srx"
    elif ansibleHandle( directory, "version_asa" ) == 0:
        #if succeeded return that the machine is a Cisco ASA
        logger.info("Success we found ASA")
        return "asa"
    elif ansibleHandle( directory, "version_ostack") == 0:
        logger.info("Success we found openstack")
        return "ostack"

    logger.critical("Error unsupported FW, or communication was not successful", returnCode)
    raise CommunicationError("Error unsupported FW, or communication was not successful")
    return "Error unsupported firewall"

######################################################




############## Setup args and argparser ##############

arg_parser = argparse.ArgumentParser(description='FIST - "Firewall rules from Identity - Simple Tranlator"')
arg_parser.add_argument('-o', default = 1, type=int, help='Integer value of the optimization level, the higher the better, currently only 1 is supported. Values should be in range <0-4> ', metavar="OPT_LEVEL")
arg_parser.add_argument('-d', help='Debug messages are printed', action='store_true')
arg_parser.add_argument('-w', help='sleep', action='store_true') #used for kill test
arg_parser.add_argument('-l', help='Usage "-l LOGFILE" logging into specific file',  type=str, metavar="LOGFILE" )
arg_parser.add_argument('-r', required=True, help='Usage "-r INPUT_FILE" file containing data from Perun (json)', type=str, metavar="INPUT_FILE")
arg_parser.add_argument('-q', help='Quiet, suppress the output (NotImplementedYet)', action='store_true')

args = arg_parser.parse_args()

############## Define logger, logfiles and stdout ###############
logger = logging.getLogger('sven-fist')
logger.setLevel(logging.DEBUG)

if (args.l is not None):
  # always write everything to the rotating log files
  log_file_handler = logging.handlers.TimedRotatingFileHandler(args.l, when='M', interval=2)
  log_file_handler.setFormatter( logging.Formatter('%(asctime)s [%(levelname)s](%(name)s:%(funcName)s:%(lineno)d): %(message)s') )
  log_file_handler.setLevel(logging.DEBUG)
  logger.addHandler(log_file_handler)


# also log to the console at a level determined by the --verbose flag
console_handler = logging.StreamHandler() # sys.stderr
#---- Stonith-like functionality (just delete previous run of fist without asking) ---
my_pid = os.getpid()
for i in pidof("fist.py"):
    if int(i)==my_pid:
        continue
    else:
        os.kill(int(i), 9) #verify that SIGKILL is ok  - cannot be skipped (SIGINT is 2 )

if( args.w ):
    os.system('sleep 10000')
if (args.d):
    console_handler.setLevel(logging.DEBUG)
else:
    console_handler.setLevel(logging.INFO)
if (args.q):
    console_handler.setLevel(logging.CRITICAL)

if( args.o < 0 ) or (args.o > 4):
    arg_parser.error("Exiting due to not supported value of argument")


console_handler.setFormatter( logging.Formatter('[%(levelname)s](%(name)s): %(message)s') )
logger.addHandler(console_handler)
logger.info( "The optimization level is %d", args.o )

# ----- end-of parse args ------
# ----- read input data (from perun) -----

with open(args.r, "r") as source:
    objects= json.load( source )
    logger.info("Input file is loaded")
# ----- end-of read input data (from perun) -----

#process
perun_sources={}
perun_rules={}

# parse input file (the perun input),
# - ((it is easier for me - Sven R. - to think about input data in this way, change this according to what suits you the best - do not forget to change everything else :) ))
# NOTE: I do not argue that it might be easier to keep data as they are loaded or change the generator of input file.
for x in objects["rules"][:]:
    if x["allowedIPs"] not in perun_sources.values():
        perun_sources['obj'+str(len(perun_sources))] = x["allowedIPs"]
    if x["rule"] not in perun_rules.values():
        perun_rules[x["rule"]] = []
    perun_rules[x["rule"]].append(list(perun_sources.keys())[list(perun_sources.values()).index(x["allowedIPs"])])
#end-of for
logger.debug("These are loaded rules from Perun: %s", perun_rules )
logger.debug("These are loaded sources from Perun: %s", perun_sources )


#connect to firewall to get All rules divided into zones (access lists)

#--------------- get FW TYPE
#get firewall type:
execDir = "./manage-fw/"

globalType = getFirewallType(execDir, logger)
#--------------------asume everything is downloaded ------------------------


#ansibleHandle(execDir, "get")

#prepare rules from perun
def openFileAsRead( path ):
    try:
        with open( path , "r" ) as source:
            openedFile = json.load( source )
        return openedFile
    except json.JSONDecodeError:
        return ""

#replace with some better way (workingDir is currently in the yml vars)
workingDir = "/tmp/perun-testing/"
if ( globalType == 'srx' ):
    ourFirewall = objs.FirewallJuniper("srx", execDir, logger, args.o )
elif ( globalType == 'asa' ):
    ourFirewall = objs.FirewallCisco("asa", execDir, logger, args.o )
elif ( globalType == 'ostack'):
    ourFirewall = objs.FirewallOpenStack("ostack", execDir, logger, args.o)


ourFirewall.getFWObjects()

#A=openFileAsRead( workingDir + "policies.json" )
#logger.debug("Data from perun: %s", A[1])
#logger.debug("%s", A)

ourFirewall.processRuleFile( openFileAsRead( workingDir + "policies.json" ) )

ourFirewall.processZoneRouteFile( openFileAsRead( workingDir + "zones.json" ), openFileAsRead( workingDir + "route-info.json" ) )
ourFirewall.processAddrBook( openFileAsRead( workingDir + "addr-book.json" ) )


perun_rules_netObj = []
perunServicesToDeploy = []
for dst in perun_rules.keys():
    ip = dst.split(':')[0]
    service = dst.split(':')[1]
    if service not in perunServicesToDeploy:
        perunServicesToDeploy.append(service)
    if ip not in perun_rules_netObj:
        perun_rules_netObj.append(ip)


logger.info( "Loading firewall services from Perun " )

ourFirewall.createServices( perunServicesToDeploy )

logger.info( "Perun net-objs from dst to Deploy: " )
ourFirewall.createNetObjectsFromDsts( perun_rules_netObj )

ourFirewall.createNetObjectsFromSource( list( perun_sources.values() ), list( perun_sources.keys() ) )

ourFirewall.deployRemote( perun_rules )
#VERIFY_COMPLETED: TODO: Add cleanup scripts (or ansible playbooks, not to be run under debug option)
if ( not args.d ):
    #CHANGE_REQ: consider this to be handled by deploy tag, unless specified to not call -> possibly cleaner solution 
    ansibleHandle(execDir, "cleanup")
    quit()
#create all services
#ansibleHandle(execDir, "deploy")
